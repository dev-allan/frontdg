import React from 'react';
import DisplayNotification from '../components/DisplayNotifications';
import Header from '../components/Header';


const Home = () => {
    return (
        <div className="home">
            <Header title="application notification"/>
            <DisplayNotification/>
        </div>
    );
};

export default Home;