import React from 'react';

const Header = (props) => {
    const { title } = props
    return (
        <div className="bg-secondary">
            <p className="d-flex justify-content-center text-uppercase fs-5 fw-bold text-light">{title}</p>
        </div>
    );
};

export default Header;