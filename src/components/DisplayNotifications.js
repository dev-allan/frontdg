import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Button, Form, Accordion } from 'react-bootstrap';

const DisplayNotifications = () => {

    const [notification, setNotification] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [title, setTitle] = useState('');
    const [theme, setTheme] = useState('');
    const [description, setDescription] = useState('');

    const deleteNotification = (id) => {
        axios.delete(`http://localhost:8080/api/notifications/${id}`);
        setRefresh(true);
    }

    const submit = e => {
        e.preventDefault();        
        axios.post("http://localhost:8080/api/notifications/", {     
        "title" : title,
        "description" : description,
        "theme" : theme 
        })
        .then(res => {
            console.log(res);
            console.log(res.data);
            setRefresh(true);
        })
    }

    const deleteAll = () => {
        axios.delete(`http://localhost:8080/api/notifications/`);
        setRefresh(true);
    }

    useEffect(() => {
        axios.get("http://localhost:8080/api/notifications/")
        .then((res) => setNotification(res.data))
        setRefresh(false);
    }, [refresh])

    return (
        <div className="notifications p-5">
            <Form>
                <Form.Group className="mb-3" controlId="formBasicText">
                    <Form.Label>Titre de la notification :</Form.Label>
                    <Form.Control type="text" placeholder="Entrez le titre" onChange={e => setTitle(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicText">
                    <Form.Label>Thème de la notification</Form.Label>
                    <Form.Control type="text" placeholder="Entrez le thème" onChange={e => setTheme(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicTextArea">
                    <Form.Label>Description de la notification</Form.Label>
                    <Form.Control as="textarea" rows={3} onChange={e => setDescription(e.target.value)} />
                </Form.Group>
                <div className="d-flex justify-content-evenly">
                    <Button className="mb-3 w-25" variant="primary" type="submit" onClick={submit}>
                        Envoyer
                    </Button>
                    <Button className="mb-3 w-25" variant="danger" type="submit" onClick={deleteAll}>
                        Supprimer tout
                    </Button>
                </div>
            </Form>
            {notification.length > 0 ?
            <Accordion className="notifications-display">
                {notification.map((notif) => (
                    <Accordion.Item key={notif.id} eventKey={notif.id}>
                        <Accordion.Header>
                            <p>Titre : {notif.title}</p>
                        </Accordion.Header>
                        <Accordion.Body> 
                            <p>Theme : {notif.theme} </p>
                            <p>Description : {notif.description}</p>
                            <div className="d-flex justify-content-end">
                                <Button variant="danger" onClick={() => deleteNotification(notif.id)}>Supprimer</Button>
                            </div>
                        </Accordion.Body>
                     </Accordion.Item>
                ))}
            </Accordion>
            :
            <p>Aucune notification</p>}
        </div>

        
    );
};

export default DisplayNotifications;